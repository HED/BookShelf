package org.testtask.restaurantmanagesupport.services;


import org.testtask.restaurantmanagesupport.dto.MustBuyListDTO;
import org.testtask.restaurantmanagesupport.entitys.MustBuyList;

import java.util.List;

public interface MustBuyListService {

    List<MustBuyList> findAll();

    MustBuyList edit(MustBuyListDTO mustBuyListDTO);

    void del(Long id);

    void delAllByMustBuy(String mustBuy);


}
