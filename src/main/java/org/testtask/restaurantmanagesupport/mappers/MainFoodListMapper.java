package org.testtask.restaurantmanagesupport.mappers;

import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dto.MainFoodListDTO;
import org.testtask.restaurantmanagesupport.entitys.MainFoodList;

@Service
public class MainFoodListMapper {

    public MainFoodListDTO convertToDTO(MainFoodList mainFoodList){
        return new MainFoodListDTO(mainFoodList.getId(),
                mainFoodList.getName(),
                mainFoodList.getInStock(),
                mainFoodList.getNeed(),
                mainFoodList.getMustBuy());
    }

    public MainFoodList convertToEntity(MainFoodListDTO mainFoodListDTO){
        return new MainFoodList(mainFoodListDTO.getId(),
                mainFoodListDTO.getName(),
                mainFoodListDTO.getInStock(),
                mainFoodListDTO.getNeed(),
                mainFoodListDTO.getMustBuy());
    }
}
