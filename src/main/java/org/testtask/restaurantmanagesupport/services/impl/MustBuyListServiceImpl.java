package org.testtask.restaurantmanagesupport.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dao.MustBuyListDAO;
import org.testtask.restaurantmanagesupport.dto.MustBuyListDTO;
import org.testtask.restaurantmanagesupport.entitys.MustBuyList;
import org.testtask.restaurantmanagesupport.mappers.MustBuyListMapper;
import org.testtask.restaurantmanagesupport.services.MustBuyListService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MustBuyListServiceImpl implements MustBuyListService {

    private MustBuyListDAO mustBuyListDao;
    private MustBuyListMapper mustBuyListMapper;
    private MustBuyList mustBuyList;
    Logger logger = LoggerFactory.getLogger(MustBuyListServiceImpl.class);

    @Autowired
    public MustBuyListServiceImpl(MustBuyListDAO mustBuyListDao, MustBuyListMapper mustBuyListMapper) {
        this.mustBuyListDao = mustBuyListDao;
        this.mustBuyListMapper = mustBuyListMapper;
    }


    @Override
    public List<MustBuyList> findAll() {
        return mustBuyListDao.findAll();
    }

    @Transactional
    @Override
    public MustBuyList edit(MustBuyListDTO mustBuyListDTO) {
        try {
            String name = mustBuyListDTO.getName();
            Long id = mustBuyListDao.findByName(name).getId();
            mustBuyListDTO.setId(id);
        }catch (NullPointerException e){
            logger.info("ERROR 404");
        }
        return mustBuyListDao.save(mustBuyListMapper.convertToEntity(mustBuyListDTO));
    }

    @Transactional
    @Override
    public void del(Long id) {
        try {
            mustBuyListDao.deleteById(id);
            logger.info("Продукт " + id + " удалён");
        } catch (EmptyResultDataAccessException e) {
            logger.error("Продукт " + id + " не удалён " + " 404");
        }
    }

    @Override
    public void delAllByMustBuy(String mustBuy) {
        try {
            Long id = mustBuyListDao.findByMustBuy(mustBuy).getId();
            mustBuyListDao.deleteById(id);
        }catch (NullPointerException|EmptyResultDataAccessException e){
            logger.error("Не найдены" + "404");
        }
    }
}
