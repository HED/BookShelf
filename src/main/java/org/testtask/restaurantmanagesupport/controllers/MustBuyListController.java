package org.testtask.restaurantmanagesupport.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.testtask.restaurantmanagesupport.entitys.MustBuyList;
import org.testtask.restaurantmanagesupport.services.MustBuyListService;
import org.testtask.restaurantmanagesupport.services.impl.MustBuyListServiceImpl;

import java.util.List;

@RestController("/mustbuylist")
public class MustBuyListController {

    private MustBuyListService service;

    @Autowired
    public MustBuyListController(MustBuyListServiceImpl service) {
        this.service = service;
    }

    Logger logger = LoggerFactory.getLogger(MainFoodListController.class);

    @GetMapping
    public List<MustBuyList> getAllFoodstuff(){
        return service.findAll();
    }

}
