package org.testtask.restaurantmanagesupport;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.testtask.restaurantmanagesupport.entitys.MainFoodList;
import org.testtask.restaurantmanagesupport.dao.MainFoodListDAO;

@SpringBootApplication
public class RestaurantManageSupportApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantManageSupportApplication.class, args);
		System.out.println("===========================================================READY===================================================================");
	}

	@Bean
	public CommandLineRunner init(MainFoodListDAO foodstuffDAOMain) {
		return args -> {
//            foodstuffDAOMain.save(new MainFoodList("Свинина", 	20,4,0));
//            foodstuffDAOMain.save(new MainFoodList("Курица", 	20,3,0));
//            foodstuffDAOMain.save(new MainFoodList("Помидоры", 	10,0,0));

		};
	}

}
