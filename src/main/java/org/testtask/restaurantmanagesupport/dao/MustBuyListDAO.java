package org.testtask.restaurantmanagesupport.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.testtask.restaurantmanagesupport.entitys.MustBuyList;

@Repository
public interface MustBuyListDAO extends JpaRepository<MustBuyList,Long> {
    MustBuyList deleteAllByMustBuy(String mustBuy);
    MustBuyList findByMustBuy(String mustBuy);
    MustBuyList findByName(String name);

}
