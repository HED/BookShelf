package org.testtask.restaurantmanagesupport.entitys;

import org.testtask.restaurantmanagesupport.errors.MyLittleError;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class MustBuyList implements MyLittleError {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String mustBuy;

    MustBuyList(){}

    public MustBuyList(String name, String mustBuy) {
        this.name = name;
        this.mustBuy = mustBuy;
    }

    public MustBuyList(Long id, String name, String mustBuy) {
        this.id = id;
        this.name = name;
        this.mustBuy = mustBuy;
    }


    @Override
    public Object error() {
        this.name    = "ERROR";
        this.mustBuy = "6666";
        return  this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMustBuy() {
        return mustBuy;
    }

    public void setMustBuy(String mustBuy) {
        this.mustBuy = mustBuy;
    }
}
