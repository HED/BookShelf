package org.testtask.restaurantmanagesupport.mappers;

import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dto.MustBuyListDTO;
import org.testtask.restaurantmanagesupport.entitys.MustBuyList;

@Service
public class MustBuyListMapper {

    public MustBuyListDTO convertToDTO(MustBuyList mustBuyList) {
        return new MustBuyListDTO(mustBuyList.getId(),
                mustBuyList.getName(),
                mustBuyList.getMustBuy());
    }

    public MustBuyList convertToEntity(MustBuyListDTO foodstuffDTO) {
        return new MustBuyList(foodstuffDTO.getId(),
                foodstuffDTO.getName(),
                foodstuffDTO.getMustBuy());
    }

}
