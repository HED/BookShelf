package org.testtask.restaurantmanagesupport.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.web.bind.annotation.*;
import org.testtask.restaurantmanagesupport.dto.MainFoodListDTO;
import org.testtask.restaurantmanagesupport.entitys.MainFoodList;
import org.testtask.restaurantmanagesupport.mappers.MainFoodListMapper;
import org.testtask.restaurantmanagesupport.services.MainFoodListService;
import org.testtask.restaurantmanagesupport.services.impl.MainFoodListServiceImpl;

import java.util.List;


@RestController
@RequestMapping("/foodlist")
public class MainFoodListController {

    private MainFoodListService service;
    private MainFoodListMapper mapper;

    @Autowired
    public MainFoodListController(MainFoodListServiceImpl service, MainFoodListMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    Logger logger = LoggerFactory.getLogger(MainFoodListController.class);

    @GetMapping
    public List<MainFoodList> getAllFoodstuff(){
        return service.getFoodstuf();
    }

    @PostMapping
    public MainFoodListDTO addFoodstuff(@RequestBody MainFoodListDTO newMainFoodList){
        logger.info("Зашел в добавление продуктов");
        return mapper.convertToDTO(service.edit(newMainFoodList));
    }


    @PutMapping
    public MainFoodListDTO editFoodstuff(@RequestBody MainFoodListDTO editMainFoodList) {
        try {
            logger.info("Продукт " + editMainFoodList.getId() + " " + editMainFoodList.getName() + " изменён ");
            editMainFoodList.setId(editMainFoodList.getId());
            return mapper.convertToDTO(service.edit(editMainFoodList));
        }catch (EmptyResultDataAccessException e) {
            logger.error("Продукт " + editMainFoodList.getId() + " " + editMainFoodList.getName() + " не изменён " +" 404");
            return editMainFoodList.error();
        }
    }


    @DeleteMapping(value = "/")
    public void delete(@RequestBody MainFoodListDTO mainFoodListDTO) {
        service.del(mainFoodListDTO.getId(), mainFoodListDTO.getName());
    }




}
