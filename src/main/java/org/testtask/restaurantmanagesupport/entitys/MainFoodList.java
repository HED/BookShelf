package org.testtask.restaurantmanagesupport.entitys;

import org.testtask.restaurantmanagesupport.errors.MyLittleError;

import javax.persistence.*;

@Entity
@Table
public class MainFoodList implements MyLittleError<MainFoodList> {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private Integer inStock;
    private Integer need;
    private String mustBuy;

    MainFoodList(){ }

    public MainFoodList(String name, Integer inStock, Integer need, String mustBuy) {
        this.name = name;
        this.inStock = inStock;
        this.need = need;
        this.mustBuy = mustBuy;
    }

    public MainFoodList(Long id, String name, Integer inStock, Integer need, String mustBuy ) {
        this.id = id;
        this.name = name;
        this.inStock = inStock;
        this.need = need;
        this.mustBuy = mustBuy;
    }

    @Override
    public MainFoodList error() {
        this.name     =  "ERROR";
        this.inStock  =  6666;
        this.need     =  6666;
        this.mustBuy  =  "6666";
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public Integer getNeed() {
        return need;
    }

    public void setNeed(Integer need) {
        this.need = need;
    }

    public String getMustBuy() {
        return mustBuy;
    }

    public void setMustBuy(String mustBuy) {
        this.mustBuy = mustBuy;
    }
}
