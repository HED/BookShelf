package org.testtask.restaurantmanagesupport.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.testtask.restaurantmanagesupport.dao.MainFoodListDAO;
import org.testtask.restaurantmanagesupport.dto.MainFoodListDTO;
import org.testtask.restaurantmanagesupport.entitys.MainFoodList;
import org.testtask.restaurantmanagesupport.entitys.MustBuyList;
import org.testtask.restaurantmanagesupport.mappers.MainFoodListMapper;
import org.testtask.restaurantmanagesupport.mappers.MustBuyListMapper;
import org.testtask.restaurantmanagesupport.services.MainFoodListService;
import org.testtask.restaurantmanagesupport.services.MustBuyListService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MainFoodListServiceImpl implements MainFoodListService {

    private MainFoodListDAO mainFoodListDAO;
    private MainFoodListMapper mainFoodListMapper;
    private MainFoodList mainFoodList;
    private MustBuyListService mustBuyListService;
    private MustBuyListMapper mustBuyListMapper;
    private MustBuyList mustBuyList;
    Logger logger = LoggerFactory.getLogger(MainFoodListServiceImpl.class);

    @Autowired
    public MainFoodListServiceImpl(MainFoodListDAO mainFoodListDAO,
                                   MainFoodListMapper mainFoodListMapper,
                                   MustBuyListServiceImpl mustBuyListServiceImpl,
                                   MustBuyListMapper mustBuyListMapper
    ){
        this.mainFoodListDAO = mainFoodListDAO;
        this.mainFoodListMapper = mainFoodListMapper;
        this.mustBuyListService = mustBuyListServiceImpl;
        this.mustBuyListMapper = mustBuyListMapper;
    }

    @Override
    public List<MainFoodList> getFoodstuf() {
        return mainFoodListDAO.findAll();
    }

    @Transactional
    @Override
    public MainFoodList edit(MainFoodListDTO mainFoodListDTO) {

        String name = mainFoodListDTO.getName();
        String mustBuy = mainFoodListDTO.getMustBuy();
        MustBuyList mustBuyList = new MustBuyList(name, mustBuy);
        mustBuyListService.edit(mustBuyListMapper.convertToDTO(mustBuyList));
        if (mainFoodListDTO.getMustBuy().equals("0")) {
            mustBuyListService.delAllByMustBuy("0");
        }

        return mainFoodListDAO.save(mainFoodListMapper.convertToEntity(mainFoodListDTO));
    }

    @Transactional
    @Override
    public void del(Long id, String name) {
        try {
            mainFoodListDAO.deleteById(id);
            logger.info("Продукт " + id + " " + name + " удалён");
        } catch (EmptyResultDataAccessException e) {
            logger.error("Продукт " + id + " " + name + " не удалён " + " 404");
        }
    }


}
