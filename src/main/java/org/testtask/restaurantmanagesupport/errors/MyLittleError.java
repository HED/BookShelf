package org.testtask.restaurantmanagesupport.errors;

public interface MyLittleError<T> {
    T error();

}
