package org.testtask.restaurantmanagesupport.services;

import org.testtask.restaurantmanagesupport.dto.MainFoodListDTO;
import org.testtask.restaurantmanagesupport.entitys.MainFoodList;

import java.util.List;

public interface MainFoodListService {

    List<MainFoodList> getFoodstuf();

    MainFoodList edit(MainFoodListDTO mainFoodListDTO);

    void del(Long id, String name);
}
