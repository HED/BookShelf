package org.testtask.restaurantmanagesupport.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.testtask.restaurantmanagesupport.entitys.MainFoodList;

@Repository
public interface MainFoodListDAO extends JpaRepository<MainFoodList,Long> {
    void deleteById(Long id);

}
